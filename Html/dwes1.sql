-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generaciÃ³n: 26-02-2020 a las 19:18:01
-- VersiÃ³n del servidor: 10.4.11-MariaDB
-- VersiÃ³n de PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dwes1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `cod` varchar(6) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`cod`, `nombre`) VALUES
('EQUI', 'Equipos'),
('PERI', 'Perifericos'),
('JUEG', 'Juegos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `id_equipo` integer(5) NOT NULL,
  `nombre_equipo` varchar(100) NOT NULL,
  `anho` integer(4) NOT NULL,
  `procesador` varchar(15) NOT NULL,
  `memoria` varchar(15) NOT NULL,
  `estado`integer(4) NOT NULL,
  `nota` text DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `grupo` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id_equipo`, `nombre_equipo`, `anho`,`procesador` ,`memoria`, `estado`, `nota`,`precio`,`grupo`) VALUES
('00001', 'Zx80', '1980', 'NEC 780C-1', '1Kb','3','sin caja ni monitor','125.00','EQUI'),
('00002', 'Zx81', '1981', 'Zilog Z80A', '8Kb','3','sin caja ni monitor','100.00','EQUI'),
('00003', 'Timex 1000', '1982', 'Zilog Z80A', '2Kb','4','en buen estado con manuales', '260.70','EQUI'),
('00004', 'Zx Spectrum +', '1984', 'Zilog Z80A', '48Kb', '9','arañazo en la esquina izquierda','98.90','EQUI'),
('00005', 'Zx Spectrum +2', '1986', 'Zilog Z80A','128Kb', '1','a estrenar con caja manuales y precintos', '1500.00','EQUI'),
('00006', 'Timex2068', '1983 ', 'Zilog Z80A','24Kb','10','con manuales y en buen estado', '640.00','EQUI'),
('00007', 'Timex1500', '1983',  'Zilog Z80A','16Kb', '4','falta un trozo de porespan de la caja''455.00','EQUI'),
('00008', 'Timex2048', '1984', 'Zilog Z80A', '48Kb', '1','falta manual de conexion', '402.00','EQUI'),
('00009', 'Sinclair PC200', '1988', 'intel 8086','512Kb', '5' 'exterior en prefecto estado', '211.00','EQUI'),
('00010', 'Sinclair QL', '1984', 'Motolola MC68008','128Kb','1','Sin caja original ni manuales', '150.00','EQUI'),
('00011', 'TK-85', '1983', 'Zilog Z80A', '16Kb', 'origen Brasil falta disco', '163.75','EQUI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `cod_estado` integer(4) NOT NULL,
  `descripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`cod_estado`, `descripcion`) VALUES
(1, 'Funcionando'),
(2, 'Sin cables'),
(3, 'Sin Caja'),
(4, 'Revisado'),
(5, 'Pendiente de revisar'),
(6, 'Falla video'),
(7, 'Falla chip'),
(8, 'Falla tecla');
(9, 'Carcasa rota');
(10, 'Completo');



-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periferico`
--

CREATE TABLE `periferico` (
  `id_periferico` integer(5) NOT NULL,
  `nombre_equipo` varchar(100) NOT NULL,
  `nombre_periferico` varchar(100) NOT NULL,
  `anho` integer(4) NOT NULL,
  `estado`integer(4) NOT NULL,
  `nota` text DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `grupo`  varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periferico`
--

INSERT INTO `periferico` (`id_periferico`,`nombre_equipo`,`nombre_periferico`, `anho`, `estado`,`nota`,`precio`,`grupo`) VALUES
('00001', 'Timex2068', 'joistick', '1983','1', 'no tiene la caja en buen estado', '23.00','PERI'),
('00002', 'Timex1000', 'pistola', '1985','1', 'falta manual', '30.00','PERI'),
('00003', 'Zx Spectrum +2', 'joistick', '1986','1', 'completo', '15.00','PERI'),
('00004', 'Zx Spectrum +2', 'unidad de disco', '1986','1', 'con manuales', '40.00','PERI');

-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `juego`
--

CREATE TABLE `juego` (
  `id_juego` integer(5) NOT NULL,
  `nombre_equipo` varchar(100) NOT NULL,
  `nombre_juego` varchar(100) NOT NULL,
  `anho` integer(4) NOT NULL,
  `estado`integer(4) NOT NULL,
  `nota` text DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `grupo`  varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volvado de datos de la tabla `juego`
--
INSERT INTO `juego` (`id_juego`,`nombre_equipo`,`nombre_juego`, `anho`, `estado`,`nota`,`precio`,`grupo`) VALUES
('00001', 'Timex2068', 'Zelda', '1986','1', 'completo', '20.00','JUEG'),
('00002', 'Timex1000', 'Tetris', '1984','1', 'falta manual', '12.00','JUEG'),
('00003', 'Zx Spectrum +2', 'PacMan', '1985','1', 'completo', '15.00','JUEG'),
('00004', 'Zx Spectrum +2', 'Asteroids', '1983','1', 'con manuales', '40.00','JUEG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(32) NOT NULL,
  `tipo` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `contrasena`, `tipo`) VALUES
('dwes1', 'e8dc8ccd5e5f9e3a54f07350ce8a2d3d','conex'),
('dark', 'deb54ffb41e085fd7f69a75b6359c989','admin');
--('dark', 'deb54ffb41e085fd7f69a75b6359c989'); --dark 1973
--('dwes1', 'e8dc8ccd5e5f9e3a54f07350ce8a2d3d',), --dwes1 abc123.


--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`cod`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id_equipo`),
  ADD UNIQUE KEY `nombre_equipo` (`nombre_equipo`),
  ADD KEY `grupo` (`grupo`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`cod_estado`),
  ADD KEY `estados_ibfk_2` (`cod_estado`);

--
-- Indices de la tabla `periferico`
--
ALTER TABLE `periferico`
  ADD PRIMARY KEY (`id_periferico`),
  ADD KEY `nombre` (`nombre_equipo`),
  ADD KEY `grupo` (`grupo`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `juego`
--
ALTER TABLE `juego`
  ADD PRIMARY KEY (`id_juego`),
  ADD KEY `nombre` (`nombre_equipo`),
  ADD KEY `grupo` (`grupo`),
  ADD KEY `estado` (`estado`);

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `cod_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


--
-- Filtros para la tabla `estados`
--
ALTER TABLE `estado`
 -- ADD CONSTRAINT `estados_ibfk_1` FOREIGN KEY (`producto`) REFERENCES `producto` (`cod`) ON UPDATE CASCADE,
  ADD CONSTRAINT `estado_ibfk_2` FOREIGN KEY (`equipo`) REFERENCES `equipo` (`estado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `estado_ibfk_2` FOREIGN KEY (`periferico`) REFERENCES `equipo` (`periferico`) ON UPDATE CASCADE,
  ADD CONSTRAINT `estado_ibfk_2` FOREIGN KEY (`juego`) REFERENCES `equipo` (`juego`) ON UPDATE CASCADE;
  
 --privilegios
CREATE USER 'dwes1'@'%' IDENTIFIED VIA mysql_native_password USING '***';GRANT ALL PRIVILEGES ON *.* TO 'dwes1'@'%' 
REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `dwes1`.* TO 'dwes1'@'%'; 



COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
