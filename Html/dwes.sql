-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generaciÃ³n: 02-11-2020 a las 19:18:01
-- VersiÃ³n del servidor: 10.4.11-MariaDB
-- VersiÃ³n de PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dwes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--

CREATE TABLE `familia` (
  `cod` int(6) NOT NULL,
  `nombre` varchar(200) NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`cod`, `nombre`) VALUES
('SPC', 'Spectrum'),
('COM', 'Comodore'),
('SEG', 'Sega'),
('NIN', 'Nintendo'),
('ATA', 'Atari'),
('SIN', 'Sinclair'),
('DARK', 'Darkminders');


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `cod` varchar(12) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `nombre_corto` varchar(50) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `PVP` decimal(10,2) NOT NULL,
  `familia` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`cod`, `nombre`, `nombre_corto`, `descripcion`, `PVP`, `familia`) VALUES
('CAMISPCN', NULL, 'camiseta SPC negra', 'Camiseta negra con el logo de Spectrum clasico', '25.00', 'SPC'),
('CAMISPCB', NULL, 'camiseta SPC blanca', 'Camiseta blanca con el logo de Spectrum clasico', '25.00', 'SPC'),
('CAMISCOMN', NULL, 'camiseta COM negra', 'Camiseta negra con el logo de Comodore clasico', '15.00', 'COM'),
('CAMISCOMB', NULL, 'camiseta COM blanca', 'Camiseta blanca con el logo de Comodore clasico', '15.00', 'COM'),
('CAMISSEGN', NULL, 'camiseta SEG negra', 'Camiseta negra con el logo de Sega clasico', '20.25', 'SEG'),
('CAMISSEGB', NULL, 'camiseta SEG blanca', 'Camiseta blanca con el logo de Sega clasico', '20.25', 'SEG'),
('CAMISNINN', NULL, 'camiseta NIN negra', 'Camiseta negra con el logo de Nintendo clasico', '20.00', 'NIN'),
('CAMISNINB', NULL, 'camiseta NIN blanca', 'Camiseta blanca con el logo de Nintendo clasico', '20.00', 'NIN'),
('CAMISATAN', NULL, 'camiseta ATA negra', 'Camiseta negra con el logo de Atari clasico', '14.00', 'ATA'),
('CAMISATAB', NULL, 'camiseta ATA blanca', 'Camiseta blanca con el logo de Atari clasico', '14.00', 'ATA'),
('CAMISSINN', NULL, 'camiseta SIN negra', 'Camiseta negra con el logo de Sinclair clasico', '12.00', 'SIN'),
('CAMISSINB', NULL, 'camiseta DIN blanca', 'Camiseta blanca con el logo de Sinclair clasico', '12.00', 'SIN'),
('CAMISDARKN', NULL, 'camiseta DARK negra', 'Camiseta negra con el logo de Nuestra Web', '28.00', 'DARK'),
('CAMISDARKB', NULL, 'camiseta DARK blanca', 'Camiseta blanca con el logo de Nuestra Web', '28.00', 'DARK'),
('TOALLADARK', NULL, 'Toalla de playa', 'CaracterÃ­sticas:\r\n\r\ntoalla de playa de algodon rizado, alta calidad con el logo de nuestra web,', '28.90', 'DARK'),
('SUDDARK', NULL, 'Sudadera', 'CaracterÃ­sticas:\r\n\r\nSudadera negra con el logo de nuestra web.\r\n\r\n\r\n', '35.30', 'DARK'),
('BADANA', NULL, 'Badana ', 'CaracterÃ­sticas:\r\badana anti-sudor.', '6.40', 'DARK');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `producto` varchar(12) NOT NULL,
  `tienda` int(11) NOT NULL,
  `unidades` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`producto`, `tienda`, `unidades`) VALUES
('CAMISPCN', 1, 26),
('CAMISPCN', 2, 19),
('CAMISPCB', 1, 50),
('BADANA', 2, 12),
('BADANA', 3, 23),
('TOALLADARK', 3, 15),
('SUDDARK', 2, 40),
('SUDDARK', 3, 20),
('SUDDARK', 1, 20),
('CAMISPCB', 2, 10),
('TOALLADARK', 2, 20),
('CAMISDARKB', 3, 10),
('CAMISATAN', 2, 2),
('CAMISATAN', 1, 1),
('CAMISSEGN', 2, 2),
('CAMISSEGN', 3, 2),
('CAMISSINB', 2, 1),
('CAMISSINB', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `cod` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `tlf` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`cod`, `nombre`, `tlf`) VALUES
(1, 'CENTRAL', '600100100'),
(2, 'SUCURSAL1', '600100200'),
(3, 'SUCURSAL2', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(32) NOT NULL,
  `tipo` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `contrasena`, `tipo`) VALUES
('dwes', 'e8dc8ccd5e5f9e3a54f07350ce8a2d3d','conex'),
('dark', 'deb54ffb41e085fd7f69a75b6359c989','admin'),
('manu', '81dc9bdb52d04dc20036dbd8313ed055','user'),
('mery', '900150983cd24fb0d6963f7d28e17f72','user');
--('dwes', 'e8dc8ccd5e5f9e3a54f07350ce8a2d3d','conex'), --dwes abc123.
--('dark', 'deb54ffb41e085fd7f69a75b6359c989'); --dark 1973
--('manu', '81dc9bdb52d04dc20036dbd8313ed055','user') --manu 1234
--('mery', '81dc9bdb52d04dc20036dbd8313ed055','user') -- mery abc123

--
-- indices para tablas volcadas
--

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`cod`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`cod`),
  ADD UNIQUE KEY `nombre_corto` (`nombre_corto`),
  ADD KEY `familia` (`familia`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`producto`,`tienda`),
  ADD KEY `stock_ibfk_2` (`tienda`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`cod`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `cod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`producto`) REFERENCES `producto` (`cod`) ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`tienda`) REFERENCES `tienda` (`cod`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
