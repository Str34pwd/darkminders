<?php
// Recuperamos la información de la sesión
session_start();
// Y comprobamos que el usuario se haya autentificado
if (!isset($_SESSION['usuario'])) {
die("Error - debe <a href='login.php'>identificarse</a>.<br />");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!--cesta.php -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
	<link rel="stylesheet" type="text/css" href="../css/Tienda.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</head>
<body class="pagcesta">
<header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
</header> 
<div class="tit"><h1><a id="sectForm"style="color:#33adff;font-size:1.5rem;">- Cesta de la compra -</a></h1></div>
<nav class="navbar navbar-expand-sm sticky-top row" >
		<div class="navbar-header col-sm-3">
			<!-- Brand -->
			<a class="navbar-brand" href="#">
				<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav col-sm-9">
				<li><a class="nav-link" href="logoff.php"><i class="fa fa-fw fa-user"></i> LogOff&nbsp;</a></li>
			</ul>
		</div>	
 </nav>	
<div class="container-fluid">
	<div id="textit">
			 <h2><span class="capitalLetter">C</span>esta de la compra</p></h2>
	</div>
	 <main>
	 <div id="flex-container">	
			<br/>
		 <div class="formul">
			</br>
			<div id="productos">
<?php
$total = 0;
const iva = 21;
foreach($_SESSION['cesta'] as $codigo => $producto) {
	echo "<p><span class='codigo'>$codigo</span>";
	echo "<span class='nombre'>${producto['nombre']}</span>";
	echo "<span class='precio'>${producto['precio']}</span></p>";
	$total = $total + $producto['precio'];
   }
	$base= round (($total*iva/100),2);
	$total=$total+$base;
	
?>
<hr/>
<p><span class='pagar'>Precio total con IVA: <?php print $total; ?> €</span></p>

	<form action='pagar.php' method='post'>
		<p>
		<span class='pagar'>
		<input type='submit' name='pagar' value='Pagar' style="color:black;"/>
		</span>
		</p>
	</form>
</div>
		</div>
	<div id="pie1">
<!--	<form action='logoff.php' method='post'>
		<input type='submit' name='desconectar' value='LogOff Usuario 
		<?php //echo $_SESSION['usuario']; ?>'/>
	</form>-->
	</div>
</div>
	</main>
	<footer>
			<div id="pie" class="container-sm p-3 my-3">
			   <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
				<!--ir a la pagina de inicio-->
			</div>
	 </footer>	
</div>

</body>
</html>

