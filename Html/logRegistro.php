<?php
$error="";
// Comprobamos si ya se ha enviado el formulario
	if (isset($_POST['enviar'])) {
	$usuario = $_POST['usuario'];
	$password = $_POST['password'];
	if (empty($usuario) || empty($password))
	$error = "Debes introducir un nombre de usuario y una contraseña";
	else {
// Comprobamos las credenciales con la base de datos
// Conectamos a la base de datos
	try {
		$opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$dsn = "mysql:host=localhost;dbname=dwes";
		$dwes = new PDO($dsn, "dwes", "abc123.", $opc);
	}
	catch (PDOException $e) {
		die("Error: " . $e->getMessage());
	}
// Ejecutamos la consulta para comprobar las credenciales
	$sql = "SELECT usuario FROM usuarios " .
		   "WHERE usuario='$usuario' " .
		   "AND contrasena='" . md5($password) . "'";
				if($resultado = $dwes->query($sql)) {
					$fila = $resultado->fetch();
					if ($fila != null) {
					// Si las credenciales existen en bd no son validas, se vuelven a pedir
					$error = "Usuario o password ya esta dado de alta!";	
					
				}
				else{
					$clave=md5($password);
				 	$sql1 = "INSERT INTO usuarios (usuario,contrasena,tipo) VALUES ('$usuario','$clave','user')"; 
					if($resultado1 = $dwes->query($sql1)) {
					$fila1 = $resultado1->fetch();
					if ($fila1 != null) {
						session_start();
						$_SESSION['usuario']=$usuario;
						header("Location: productos.php");}
					
					}
					unset($resultado1);
					unset($resultado);
				}
			unset($dwes);	
	}
	}}	
?>  
	
				


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!-- Tienda Web: logregistro.php -->
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Login.css" title="style" />
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
	<link rel="stylesheet" type="text/css" href="../css/Contacto.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</head>
<body>
  
	
   <header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
    </header> 
    <div class="tit"><h1><a id="sectForm"style="color:#33adff;font-size:1.5rem;">- Registro -</a></h1></div>
 <nav class="navbar navbar-expand-sm sticky-top row" >

    <div class="navbar-header col-sm-2">
	    <!-- Brand -->
		<a class="navbar-brand" href="#">
			<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
		</a>
		<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
     </div>
	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	 <ul class="navbar-nav col-sm-9">
	   
		<li><a class="nav-link" href="Tienda.html"><i class="fa fa-fw fa-cart-plus"></i>  Volver sin Registrarte&nbsp;</a></li>
	 </ul>
	 </div>
 </nav>
 <div class="container-fluid">
    <div id="textit">
         <h2><span class="capitalLetter">R</span>egistrate para comenzar a comprar</h2>
    </div>
  <main>

  <div id="flex-container">	
	<br/>
	<div class="formul">
	
	</br>
		<div id='logon'>
		<form action='logRegistro.php' method='post'>
			<fieldset >
				
				<div><span class='error'><?php echo $error; ?></span></div>
				<div class='campo'>
				<label for='usuario' >Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<input type='text' name='usuario' id='usuario' maxlength="50" /><br/>
				</div><br/>
				<div class='campo'>
				<label for='password' >Contraseña:</label>
				<input type='password' name='password' id='password' maxlength="50" /><br/>
				</div><br/>
				<div class='campo'>
				<label for='password' >Repetir Contraseña:</label>
				<input type='password' name='repassword' id='password' maxlength="50" /><br/>
				</div>
				<br/><br/>
				<div class='campo'>
				<input type='submit' name='enviar' value='Registrar' style="color:black;"/><br/>
				<br/><br/>
				</div>
			</fieldset>
		</form>
	</div>
	 </div>
    <aside>
                        
    </aside>
 </div>
  </main>
 <footer>
        <div id="pie" class="container-sm p-3 my-3">
		    <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
            <!--ir a la pagina de inicio-->
        </div>
    </footer>
</div>
</body>

</html>