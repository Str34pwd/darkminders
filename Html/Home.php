<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Home.css" title="style" />
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
	<script>baguetteBox.run('.tz-gallery'); </script>  
</head>
<?php
			/// index.php
				@session_start();

			// If user is logged in, retrieve identity from session.
				$identity = null;
				if (isset($_SESSION['identity'])) {
				$identity = $_SESSION['identity'];
				}
?>
<body>
	<form id="form_seleccion" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	</form>
   <header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
    </header> 
    <div class="tit"><h1><a id="sectForm"style="color:#33adff; font-size:1.5rem;">- Inicio -</a></h1></div>
    <nav class="navbar navbar-expand-sm sticky-top row" >

		<div class="navbar-header col-sm-3">
			<!-- Brand -->
			<a class="navbar-brand" href="#">
			<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
		
			</button>
		</div>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav col-sm-9">
                <!--<li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">☰</a></li>-->
                <li><a class="nav-link active" href="Home.php"><i class="fa fa-fw fa-home"></i> Inicio&nbsp;</a></li>
                <li><a class="nav-link" href="Historia.html"><i class="fa fa-fw fa-book"></i> Historia&nbsp;</a></li>
				<li><a class="nav-link" href="Contacto.html"><i class="fa fa-fw fa-envelope"></i> Contacto&nbsp;</a></li>
                <li><a class="nav-link" href="Dispositivos.html"><i class="fa fa-fw fa-hdd-o"></i> Dispositivos&nbsp;</a></li>
                <li><a class="nav-link" href="Videos.html"><i class="fa fa-fw fa-tv"></i> Videos&nbsp;</a></li>
				<li><a class="nav-link" href="Tienda.html"><i class="fa fa-fw fa-cart-plus"></i> Comprar&nbsp;</a></li>
				<li><a class="nav-link" href="Login.php"><i class="fa fa-fw fa-database"></i> Administrar&nbsp;</a></li>
				<li><FORM method=GET action="http://www.google.es/search" target="_blank">
				<fieldset>
					<input type=hidden name=ie value=UTF-8 />
					<input type=hidden name=oe value=UTF-8 />
					<INPUT TYPE=text id="s" name="q" value=""/>
					<input type=hidden name=domains value="http://www.google.com"/>
					<button type="submit" class="btn btn-outline fa fa-search" style="color:white;"></button>
				</fieldset>
				</form>	</li>	
			</ul>
			</div>
    </nav>

 <div class="container-fluid">
    <div id="textit">
        <h2><span class="capitalLetter">A</span>ctividades relacionadas con las viejas computadoras y consolas,</h2>
        <h2>en los 20 años anteriores al triunfo del PC ...</h2>
    </div>

    <main>
        <div id="galeria">
		  <div class="row">
            <div class="col elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/ZX80.webp">
                    <img src="../imagenes/Galeria/ZX80.webp" alt="ZX80" />
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
				<a class="lightbox" href="../imagenes/Galeria/Timex1500.webp">
                    <img src="../imagenes/Galeria/Timex1500.webp" alt="Timex1500" />
                </a>
                
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Zx+2.webp">
                    <img src="../imagenes/Galeria/Zx+2.webp" alt="Zx+2" />
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Zx128Esp.webp">
                    <img src="../imagenes/Galeria/Zx128Esp.webp" alt="Zx128Esp" />
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Z88.webp">
                    <img src="../imagenes/Galeria/Z88.webp" alt="Z88" />
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Zx+.webp">
                    <img src="../imagenes/Galeria/Zx+.webp" alt="Zx+" />
                </a>
            </div>
			

			<div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Zx+2A.webp">
                    <img src="../imagenes/Galeria/Zx+2A.webp" alt="Zx+2A"/>
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Zx+Esp.webp">
                    <img src="../imagenes/Galeria/Zx+3Esp.webp" alt="Zx+Esp" />
                </a>
            </div>
			<div class="elemento col-sm-2 col-md-3">
			    <a class="lightbox" href="../imagenes/Galeria/Zx128.webp">
                    <img src="../imagenes/Galeria/Zx128.webp" alt="Zx128" />
                </a>
                
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Timex2068.webp">
                    <img src="../imagenes/Galeria/Timex2068.webp" alt="Timex2068" />
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/Timex2048.webp">
                    <img src="../imagenes/Galeria/Timex2048.webp" alt= "Timex2048"/>
                </a>
            </div>
            <div class="elemento col-sm-2 col-md-3">
                <a class="lightbox" href="../imagenes/Galeria/ZX81.webp">
                    <img src="../imagenes/Galeria/ZX81.webp" alt="ZX81" />
                </a>
            </div>
          </div>
		</div>

        <aside>
                        
        </aside>

    </main>

    <footer>
        <div id="pie" class="container-sm p-3 my-3">
            <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
            <!--ir a la pagina de inicio-->
        </div>
    </footer>
</div>
</body>

</html>