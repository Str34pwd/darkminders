<?php
$error="";
// Comprobamos si ya se ha enviado el formulario
	if (isset($_POST['enviar'])) {
	$usuario = $_POST['usuario'];
	$password = $_POST['password'];
	if (empty($usuario) || empty($password))
	$error = "Debes introducir un nombre de usuario y una contraseña";
	else {
// Comprobamos las credenciales con la base de datos
// Conectamos a la base de datos
	try {
		$opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$dsn = "mysql:host=localhost;dbname=dwes1";
		$dwes1 = new PDO($dsn, "dwes1", "abc123.", $opc);
	}
	catch (PDOException $e) {
		die("Error: " . $e->getMessage());
	}


// Ejecutamos la consulta para comprobar las credenciales
	$sql = "SELECT usuario FROM usuarios " .
		   "WHERE usuario='$usuario' " .
		   "AND contrasena='" . md5($password) . "'" .
		    "AND tipo='admin'";
				if($resultado = $dwes1->query($sql)) {
						$fila = $resultado->fetch();
						if ($fila != null) {
								session_start();
								$_SESSION['usuario']=$usuario;
								header("Location: admin.php");
				}
				else {
				// Si las credenciales no son validas, se vuelven a pedir
				$error = "Usuario o password no validos para administrar! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Introduce un usuario administrador";
				}
				unset($resultado);
				}
				unset($dwes);
	}
	}
?>
<!DOCTYPE html>

<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Login.css" title="style" />
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
	 <link rel="stylesheet" type="text/css" href="../css/Contacto.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
  </head>


  <body>
	<header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
    </header> 

	 <div class="tit"><h1><a id="sectForm"style="color:#33adff; font-size:1.5rem;">- Administrar -</a></h1></div>
      <nav class="navbar navbar-expand-sm sticky-top row" >

		<div class="navbar-header col-sm-3">
	    <!-- Brand -->
			<a class="navbar-brand" href="#">
			<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav col-sm-9">
			
            	<li><a class="nav-link" href="Logout.php"><i class="fa fa-fw fa-user-times"></i>Logout&nbsp;</a></li>
			</ul>           
		</div>					
	  </nav>

	  <div class="container-fluid">
		<div id="textit">
			<h2><span class="capitalLetter">L</span>ogate para administrar la BD...</h2>
		</div>
		<main>

	<div id="flex-container">	
	<br/>
		<div class="formul">
		</br>
		<form action='login.php' method='post'>
		 <fieldset>
			<div><span class='error'><?php echo $error; ?></span></div>
			<div class='campo'>
				<label for='usuario' >Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<input type='text' name='usuario' id='usuario' maxlength="50" /><br/><br/>
			</div>
			<div class='campo'>
				<label for='password' >Password:&nbsp;&nbsp;</label>
				<input type='password' name='password' id='password' maxlength="50" /><br/><br/>
			</div>
			<div class='campo'>
				<input type='submit' name='enviar' value='Enviar' style="color:black;"/><br/>
				<input type="button" id="enviar" value="olvide el password" onclick="'mailto:darkminders@yahoo.com''subject=olvido&body=recordatorio-Admin-1234'"  method="post" enctype="text/plain">
				<br/><br/>
			</div>
		 </fieldset>
		</form>
		</div>

		</br>
		<aside>
                        
		</aside>
		</div>
	</main>	
		<footer>
			<div id="pie" class="container-sm p-3 my-3">
				<a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
				<!--ir a la pagina de inicio-->
			</div>
		</footer>
	</div>		
  </body>
</html>
  