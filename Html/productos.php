<?php
// Recuperamos la información de la sesión
	session_start();
// Y comprobamos que el usuario se haya autentificado
		if (!isset($_SESSION['usuario'])) {
		die("Error - debe <a href='logon.php'>identificarse</a>.<br />");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
	<link rel="stylesheet" type="text/css" href="../css/Tienda.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</head>
<body class="pagproductos">
	<header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
    </header> 
	<div class="tit"><h1><a id="sectForm"style="color:#33adff;font-size:1.5rem;">- Listado de productos -</a></h1></div>
	<nav class="navbar navbar-expand-sm sticky-top row" >
		<div class="navbar-header col-sm-3">
			<!-- Brand -->
			<a class="navbar-brand" href="#">
				<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		   <ul class="navbar-nav col-sm-9">
			 <li><a class="nav-link" href="Tienda.html"><i class="fa fa-fw fa-cart-plus"></i>  Volver sin Comprar&nbsp;</a></li>			           				
			</ul>
		</div>	
	</nav>	
<div class="container-fluid">
    <div id="textit">
         <h2><span class="capitalLetter">E</span>lige los productos en la lista</p></h2>
    </div>
	<main>	
		<div id="flex-container">	
			<br/>
		 <div class="formul">
			</br>
			<div id="cesta">
				<h2><img src="..\imagenes\cesta.jpg" alt="Cesta" width="24" height="21"> Cesta</h2>
				<hr />

<?php
					// Comprobamos si se ha enviado el formulario de vaciar la cesta
							if (isset($_POST['vaciar'])) {
								unset($_SESSION['cesta']);
							}
							// Comprobamos si se ha enviado el formulario de añadir
							if (isset($_POST['enviar'])) {
							// Creamos un array con los datos del nuevo producto
								$producto['nombre'] = $_POST['nombre'];
								$producto['precio'] = $_POST['precio'];
							// y lo anhadimos
								$_SESSION['cesta'][$_POST['producto']] = $producto;
							}
						// Si la cesta esta vacia, mostramos un mensaje
							$cesta_vacia = true;
							if (empty($_SESSION['cesta'])) {
								print "<p>Cesta vacia</p>";
							}
						// Si no esta vacia, mostrar su contenido
							else {
								foreach ($_SESSION['cesta'] as $codigo => $producto)
									print "<p>$codigo</p>";
								$cesta_vacia = false;
							}
?>
					<form id='vaciar' action='productos.php' method='post'>

						<input type='submit' name='vaciar' value='Vaciar Cesta'
<?php if ($cesta_vacia) print "disabled='true'"; ?>
						/>
					</form>
					<form id='comprar' action='cesta.php' method='post'>
						<input type='submit' name='comprar' value='Comprar'
<?php if ($cesta_vacia) print "disabled='true'"; ?>
						/>
					</form>
			</div>
			<div id="productos">
<?php
					try {
						$opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
						$dsn = "mysql:host=localhost;dbname=dwes";
						$dwes = new PDO($dsn, "dwes", "abc123.", $opc);
					}
					catch (PDOException $e) {
						$error = $e->getCode();
						$mensaje = $e->getMessage();
					}
					if (!isset($error)) {
						$sql = "SELECT cod, nombre_corto, PVP, Imagen FROM producto";
									$resultado = $dwes->query($sql);
								if($resultado) {
					// Creamos un formulario por cada producto obtenido
									$row = $resultado->fetch();
									while ($row != null) {
									echo "<p><h3><form id='${row['cod']}' action='productos.php' method='post'>";
									// Metemos ocultos los datos de los productos
									echo "<input type='hidden' name='producto' value='".$row['cod']."'/>";
									echo "<input type='hidden' name='nombre' value='".$row['nombre_corto']."'/>";
									echo "<input type='hidden' name='precio' value='".$row['PVP']."'/>";
									echo "<input type='hidden' name='imagen' value='".$row['Imagen']."'/>";
									echo "<input type='submit' name='enviar' value='Añadir Producto'/>";
									echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									echo " ${row['nombre_corto']}: ";
									echo $row['PVP']." euros.";
									echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									echo "<img src='../imagenes/Productos/";
									echo $row['Imagen']." '>";
									echo "</form>";
									echo "</h3></p>";
									$row = $resultado->fetch();
									}
								}
					}

?>
			</div>
			<!--<br class="divisor" />-->
		</div>	
			 <div id="pie1">
				<form action='logoff.php' method='post'>
					 <input type='submit' name='desconectar' value='Desconectar usuario >
<?php echo $_SESSION['usuario'];?>		
				</form>
			
<?php
						if (isset($error)) {
						print "<p class='error'>Error $error: $mensaje</p>";
						}
?>
			</div>
		</div>
	
	</main>
	<footer>
        <div id="pie" class="container-sm p-3 my-3">
		   <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
            <!--ir a la pagina de inicio-->
        </div>
	</footer>	
</div>
</body>
</html>
