
<!DOCTYPE html>

<html lang="es">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Login.css" title="style" />
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script> // jQuery
        $(document).ready(function(){ 
			$('#ver_equipo').on('click',function(){
             $('#formule').show();
			 $('#formulp').hide();
			 $('#formulj').hide();
            });
         });</script>
	<script> // jQuery
        $(document).ready(function(){ 
			$('#ver_periferico').on('click',function(){
             $('#formule').hide();
			 $('#formulp').show();
			 $('#formulj').hide();
            });
         });</script>
	<script> // jQuery
        $(document).ready(function(){ 
			$('#ver_juego').on('click',function(){
             $('#formule').hide();
			 $('#formulp').hide();
			 $('#formulj').show();
            });
         });</script>	 

</head>
        <?php
        /// index.php
		@session_start();

		// If user is logged in, retrieve identity from session.
		$identity = null;
		if (isset($_SESSION['identity'])) {
			$identity = $_SESSION['identity'];
		}
		?>
	
	<body>
	  <form id="form_seleccion" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	  </form>
	   <header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
      </header> 
	  <div class="tit"><h1><a id="sectForm"style="color:#33adff;font-size:1.5rem;">- Administrar -</a></h1></div>
	  <nav class="navbar navbar-expand-sm sticky-top row" >

		<div class="navbar-header col-sm-3">
			<!-- Brand -->
			<a class="navbar-brand" href="#">
				<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
		</div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
		       <ul class="navbar-nav col-sm-9">
		             <li><a class="nav-link" href="logout.php"><i class="fa fa-fw fa-user-times"></i>Logout&nbsp;</a></li>
		             <li class="nav-item dropdown">
					      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Listados&nbsp;</a>
							<div class="dropdown-menu">
							         <a class="dropdown-item" href="#" id="ver_equipo">Equipos&nbsp;</a>
									 <a class="dropdown-item" href="#" id="ver_periferico">Perifericos&nbsp;</a>
									 <a class="dropdown-item" href="#" id="ver_juego">Juegos&nbsp;</a>								
							</div>
					 </li>		 
					 <br/>
					 <h3><strong> Bienvenido <?= $identity ?></strong></h3>  
				 </ul>	 
		</div>	
      </nav>
<div class="container-fluid">
    <div id="textit">
         <h2><span class="<span class="capitalLetter">B</span>ienvenido administrador, si  deseas consultar tu coleccion este es el lugar</p>
		 <p>Elige que listado deseas consultar...</p></h2>
    </div>	  
	
	    <main>	
			<div id="flex-container">
			<br/>
				<?php
				function connectDb() {
					$server = "localhost";
					$user = "dwes1";
					$pass = "abc123.";
					$bd = "dwes1";
					@$conexion = mysqli_connect($server, $user, $pass, $bd) or die("ERROR AL CONECTARSE CON LA BASE DE DATOS");
					return $conexion;
				}
				?>
			<div class="formule" id="formule" style="display:none">
			<div class="tit"><h1><a id="sectForm">- Equipos -</a></h1>
				<div id=equipos>
			<?php
				
				/* obtenemos la conexion a la base de datos */
				$con = connectDb();
				$time = time(); //obtenemos la fecha y la hora para almacenarla en las observaciones
				
				//escribimos la query para seleccionar los campos que queremos de las tablas
				$q = "SELECT a.nombre_equipo,a.anho,a.procesador,a.memoria,b.descripcion,a.nota,a.precio,a.grupo from equipo a, estado b where a.estado=b.cod_estado";
				$result = mysqli_query($con, $q); //almacenamos la consulta
				echo "<div style='overflow-x:auto;'>";
				echo "<table border=1>";
				echo"<tr>";
				//echo"<td class=cabeza>CODIGO</td>";
				echo"<td class=cabeza>DESCRIPCION</td> \n";
				echo"<td class=cabeza>AÑO</td> \n";
				echo"<td class=cabeza>PROCESADOR</td> \n";
				echo"<td class=cabeza>MEMORIA</td> \n";
				echo"<td class=cabeza>ESTADO</td> \n";
				echo"<td class=cabeza>NOTA</td> \n";
				echo"<td class=cabeza>PRECIO</td>\n";
				echo"<td class=cabeza>GRUPO</td><tr> \n";
   
				while ($rs = mysqli_fetch_array($result)) { /* mientras tengamos datos en la consulta los almacenamos  par despues llamar al codigo en el html */
					//$codigo = $rs['id_equipo'];
					$descripcion = $rs['nombre_equipo'];
					$anho = $rs['anho'];
					$procesador = $rs['procesador'];
					$memoria = $rs['memoria'];
					$estado=$rs['descripcion'];
					$nota = $rs['nota'];
					$precio = $rs['precio'];
					$grupo = $rs['grupo'];
					
				//echo "<td class=tabla>" . $codigo . "</td> \n";
				echo "<td class=tabla >" . $descripcion . "</td> \n";
				echo "<td class=tabla>" . $anho . "</td> \n";
				echo "<td class=tabla>" . $procesador . "</td> \n";
				echo "<td class=tabla>" . $memoria . "</td> \n";
				echo "<td class=tabla>" . $estado  . "</td> \n";
				echo "<td class=tabla>" . $nota  . "</td> \n";
				echo "<td class=tabla>" . $precio . "</td> \n";	
				echo "<td class=tabla>" . $grupo . "</td> \n";	
			   //echo "<td> <a href='edit.php?id=$id_al' id='idal'>Editar</a></td>"; /* creamos un boton en la tabla por si queremos editar las observaciones */-->
				echo "</tr> \n";
				}
				echo "</table></div>";
				mysqli_free_result($result);
				mysqli_close($con);
				?>
			</div>
			
		</div>
	</div>	
	<div class="formulp"id="formulp" style="display:none">
			<div class="tit"><h1><a id="sectForm">- Perifericos -</a></h1>
			
				<div id=perifericos>
			<?php
				
				/* obtenemos la conexion a la base de datos */
				$con1 = connectDb();
				$time = time(); //obtenemos la fecha y la hora para almacenarla en las observaciones
				//$id_prod = isset($_GET['id']) ?$_GET['id']: NULL ; /* obtenemos el id que nos paso el enlace del archivo mostrar.php */
				//$q1 = "SELECT * FROM periferico"; //seleccionamos todos los campos de la tabla 
				
				$q1 = "SELECT a.nombre_equipo,a.nombre_periferico,a.anho,b.descripcion,a.nota,a.precio,a.grupo from periferico a, estado b where a.estado=b.cod_estado";
				$result1 = mysqli_query($con1, $q1); //almacenamos la consulta
				echo "<div style='overflow-x:auto;'>";
				echo "<table border=1>";
				echo"<tr>";
				echo"<td class=cabeza>EQUIPO ASOCIADO</td>";
				echo"<td class=cabeza>PERIFERICO</td> \n";
				echo"<td class=cabeza>AÑO</td> \n";
				echo"<td class=cabeza>ESTADO</td> \n";
				echo"<td class=cabeza>NOTA</td> \n";
				echo"<td class=cabeza>PRECIO</td>\n";
				echo"<td class=cabeza>GRUPO</td><tr> \n";
   
				while ($rs1 = mysqli_fetch_array($result1)) { /* mientras tengamos datos en la consulta los almacenamos  par despues llamar al codigo en el html */
					$codigo = $rs1['nombre_equipo'];
					$descripcion = $rs1['nombre_periferico'];
					$anho = $rs1['anho'];
					$estado=$rs1['descripcion'];
					$nota = $rs1['nota'];
					$precio = $rs1['precio'];
					$grupo = $rs1['grupo'];
					
				echo "<td class=tabla>" . $codigo . "</td> \n";
				echo "<td class=tabla>" . $descripcion . "</td> \n";
				echo "<td class=tabla>" . $anho . "</td> \n";
				echo "<td class=tabla>" . $estado  . "</td> \n";
				echo "<td class=tabla>" . $nota  . "</td> \n";
				echo "<td class=tabla>" . $precio . "</td> \n";	
				echo "<td class=tabla>" . $grupo . "</td> \n";	
			   //echo "<td> <a href='edit.php?id=$id_al' id='idal'>Editar</a></td>"; /* creamos un boton en la tabla por si queremos editar las observaciones */-->
				echo "</tr> \n";
				}
				echo "</table></div>";
				mysqli_free_result($result1);
				mysqli_close($con1);
				?>
			</div>
			
		</div>
	</div>	
	<div class="formulj" id="formulj" style="display:none">
			<div class="tit"><h1><a id="sectForm">- Juegos -</a></h1>
			
				<div id=juegos>
			<?php
				
				/* obtenemos la conexion a la base de datos */
				$con2 = connectDb();
				$time = time(); //obtenemos la fecha y la hora para almacenarla en las observaciones
				//$id_prod = isset($_GET['id']) ?$_GET['id']: NULL ; /* obtenemos el id que nos paso el enlace del archivo mostrar.php */
				//$q = "SELECT * FROM equipo"; //seleccionamos todos los campos de la tabla 
				
				$q2 = "SELECT a.nombre_equipo,a.nombre_juego,a.anho,b.descripcion,a.nota,a.precio,a.grupo from juego a, estado b where a.estado=b.cod_estado";
				$result2 = mysqli_query($con2, $q2); //almacenamos la consulta
				echo "<div style='overflow-x:auto;'>";
				echo "<table border=1>";
				echo"<tr>";
				echo"<td class=cabeza>EQUIPO ASOCIADO</td>";
				echo"<td class=cabeza>JUEGO</td> \n";
				echo"<td class=cabeza>AÑO</td> \n";
				echo"<td class=cabeza>ESTADO</td> \n";
				echo"<td class=cabeza>NOTA</td> \n";
				echo"<td class=cabeza>PRECIO</td>\n";
				echo"<td class=cabeza>GRUPO</td><tr> \n";
   
				while ($rs2 = mysqli_fetch_array($result2)) { /* mientras tengamos datos en la consulta los almacenamos  par despues llamar al codigo en el html */
					$codigo = $rs2['nombre_equipo'];
					$descripcion = $rs2['nombre_juego'];
					$anho = $rs2['anho'];
					$estado=$rs2['descripcion'];
					$nota = $rs2['nota'];
					$precio = $rs2['precio'];
					$grupo = $rs2['grupo'];
					
				echo "<td class=tabla>" . $codigo . "</td> \n";
				echo "<td class=tabla>" . $descripcion . "</td> \n";
				echo "<td class=tabla>" . $anho . "</td> \n";
				echo "<td class=tabla>" . $estado  . "</td> \n";
				echo "<td class=tabla>" . $nota  . "</td> \n";
				echo "<td class=tabla>" . $precio . "</td> \n";	
				echo "<td class=tabla>" . $grupo . "</td> \n";	
			   //echo "<td> <a href='edit.php?id=$id_al' id='idal'>Editar</a></td>"; /* creamos un boton en la tabla por si queremos editar las observaciones */-->
				echo "</tr> \n";
				}
				echo "</table></div>";
				mysqli_free_result($result2);
				mysqli_close($con2);
				?>
			</div>
			
		</div>
	</div>	
</div>
 </main>
	<footer>
        <div id="pie" class="container-sm p-3 my-3">
		   <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
            <!--ir a la pagina de inicio-->
        </div>
	</footer>
</div>
	</body>

</html>
  
