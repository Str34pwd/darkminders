<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="../css/Pagar.css" title="style" />
    <link rel="stylesheet" type="text/css" href="../css/Comunes.css" title="style" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
</head>

<body>
   <header>
        <div id="logo">
            <picture>
                <source class="logo2" aria-label="logo" media="(min-width: 768px)" srcset="../imagenes/Logo/Logo.jpg">
                <source class="logo2" aria-label="logo" media="(min-width: 300px)" srcset="../imagenes/Logo/Loguito_0.jpg">
                <img class="logo2" src="../imagenes/Logo/Dark.jpg" alt="logo">
				<br>
            </picture>
        </div>
    </header> 

    <div class="tit"><h1><a id="sectForm"style="color:#33adff;font-size:1.5rem;">- Pago -</a></h1></div>
<nav class="navbar navbar-expand-sm sticky-top row" >

		<div class="navbar-header col-sm-3">
			<!-- Brand -->
			<a class="navbar-brand" href="#">
				<img src="../imagenes/Logo/Dark.jpg" class="rounded-circle"  alt="Nombre" style="width:130px;" >
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			 <span class="icon-bar">☰</span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav col-sm-9">
				<li><a class="nav-link" href="logoff.php"><i class="fa fa-fw fa-user"></i> LogOff&nbsp;</a></li>	
			</ul>
		</div>
 </nav>	
<div class="container-fluid">
	<div id="textit">
			 <h2><span class="capitalLetter">E</span>stamos procesando tu pedido.......</p></h2>
	</div>
    <main>
		 <div id="flex-container">	
			<br/>
		  <div class="formul">
			</br>
            
              <form>
			   <div class="con">
					<div class="head-form">
					  <h2>Pagar</h2>
					  <p>Introduce los datos de la tarjeta</p>
					</div>
					<div class="field-set">
                       <label for="numTarj">Numero de Tarjeta</label>
                       <input id="numTarj" name="numTarj" placeholder="XXXX-XXXX-XXXX-XXX" required><br>
                       <label for="nombreTarj">Nombre Tarjeta</label>
                       <input id="nombreTarj" name="nombreTarj" placeholder=""><br>
                       <label for="fCad">Validez Tarjeta</label>
                       <input id="date" name="fCad" placeholder=""><br>
                       <label for="cvc">CVC</label>
                       <input id="cvc" name="cvc" placeholder="CVC"><br></br></br>
					   <input type="submit" class="btn-submits-pago display-4" onclick="alert('...MUCHAS GRACIAS POR TU COMPRA. RECIBIRÁS UN MAIL CON LOS DATOS DE ENTREGA. Deslogate para finalizar')"value="CONFIRMAR PAGO"/>
					  
                      <!-- <button class="btn-submits-pago display-4" >CONFIRMAR PAGO</button>
					   
					   <div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>...MUCHAS GRACIAS POR TU COMPRA.</strong> RECIBIRÁS UN MAIL CON LOS DATOS DE ENTREGA.
						</div>-->
							 
					</div>
				</div>
			   </form>
			
				</br></br>
		  </div>	
			<aside>
				</br></br></br>
			</aside>
		</div>	
	</main>
		<br/><br/><br/>
    <footer>
        <div id="pie">
            
            <a class="active" href="Home.php"><i class="fa fa-fw fa-home"></i> Home</a>
            <!--ir a la pagina de inicio-->
        </div>
    </footer>
</div>
</body>

</html>
	








